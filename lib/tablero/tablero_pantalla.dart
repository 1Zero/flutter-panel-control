import 'package:escritorioejemplo/constant.dart';
import 'package:escritorioejemplo/models/recentfile.dart';
import 'package:escritorioejemplo/responsive.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../componentes/componente_encabezado.dart';
import '../componentes/mifield.dart';
import '../componentes/storage_details.dart';




class TableroPantalla extends StatelessWidget {
  const TableroPantalla({super.key});

  @override
  Widget build(BuildContext context) {
   
    return SafeArea(
      child: SingleChildScrollView(
        padding: const EdgeInsets.all(defaultPadding),
        child: Column(
          children: [
            const Encabezado(),
            const SizedBox(height: defaultPadding,),

            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                   flex:5,
                    child: Column(
                      children: [
                        const mifield(),
                        const SizedBox(height: defaultPadding),
                          recentFile(context),
                           if(Responsive.isMobile(context))
                          SizedBox(width: defaultPadding,),  
                          if(Responsive.isMobile(context)) storagedetails()
                      ]
                    ),
                  ),
                  if(!Responsive.isMobile(context))
                   SizedBox(width: defaultPadding,),  
                   if(!Responsive.isMobile(context))
                 Expanded(
                    flex:2,
                    child: storagedetails()
                  )  

              ],
            )
             
          ]
        ),
      ),
    );
  }

  Container recentFile(BuildContext context) {
    return Container(
                            padding: const EdgeInsets.all(defaultPadding),
                            decoration: const BoxDecoration(
                            color:secondaryColor,
                            borderRadius: BorderRadius.all(Radius.circular(10)) 
                          ),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('archivos recientes',
                                style: Theme.of(context).textTheme.subtitle1,
                                ),
                                SizedBox(
                                  width: double.infinity,
                                  child: DataTable(
                                      horizontalMargin: 0,
                                      columnSpacing:defaultPadding ,
                                      columns:const [
                                        DataColumn(
                                            label: Text('nombre archivo')
                                          ),
                                        DataColumn(
                                            label: Text('fecha')
                                          ),
                                        DataColumn(
                                            label: Text('tamaño')
                                          )    
                                      ], 
                                      rows: List.generate(
                                          demoRecentFiles.length, (index) => 
                                          recentFileDataRow(demoRecentFiles[index])
                                        )
                                    ),
                                )
                              ]
                            ),
                      );
  }

  DataRow recentFileDataRow(RecentFile fileInfo) {
    return DataRow(
                                          cells: [
                                            DataCell(                                                
                                              Row(
                                                children: [
                                                  SvgPicture.asset(
                                                      fileInfo.icon!,
                                                      height: 30,
                                                      width: 30,
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
                                                      child: Text(fileInfo.title!),
                                                    )
                                                ],
                                              )
                                            ),
                                            DataCell(Text(fileInfo.date!)),
                                            DataCell(Text(fileInfo.size!))

                                          ]
                                        );
  }

}


