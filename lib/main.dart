import 'package:escritorioejemplo/constant.dart';
import 'package:escritorioejemplo/controller/menu_controller.dart';
import 'package:escritorioejemplo/pantalla_principal.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

void main(){
  runApp(MyApp());
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Panel Administrativo',
      theme: ThemeData.dark().copyWith(
        scaffoldBackgroundColor: bgColor,
        textTheme: GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme)
        .apply(bodyColor: Colors.white),
        canvasColor: secondaryColor
      ),
      home: MultiProvider(providers: [ChangeNotifierProvider(
            create: (context ) =>MenuController()
          )
        ],
        child: PantallaInicio(),
      ),
    );
  }
}