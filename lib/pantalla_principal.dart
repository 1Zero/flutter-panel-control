import 'package:escritorioejemplo/controller/menu_controller.dart';
import 'package:escritorioejemplo/responsive.dart';
import 'package:escritorioejemplo/tablero/tablero_pantalla.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'componentes/side_menu.dart';

class PantallaInicio extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      key: context.read<MenuController>().scaffoldKey,
      drawer: SideMenu(),
      body: SafeArea(
          child:Row(
            crossAxisAlignment: CrossAxisAlignment.start,
              children:  [
                if(Responsive.isDesktop(context))
               Expanded(
                   child: SideMenu()
                  ),
                  Expanded(
                      flex:  5,
                      child: TableroPantalla()
                    )
              ],
            ) 
        ),
    );
  }
}

