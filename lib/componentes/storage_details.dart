import 'package:escritorioejemplo/componentes/storage_info.dart';
import 'package:flutter/material.dart';

import '../constant.dart';
import 'chart.dart';

class storagedetails extends StatelessWidget {
  const storagedetails({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(defaultPadding),
                         
      decoration: const BoxDecoration(
          color:secondaryColor,
          borderRadius: BorderRadius.all(Radius.circular(10)), 
        ),
        child: Column(
            children: [
              const Text(
                'detalles de almacenamiento',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500
                ),
              ),
             const SizedBox(height: defaultPadding,),
              chart(context),
              const StorageInfo(
                SvgSrc:'assets/icons/Documents.svg' ,
                title: 'Documentos',
                amountOfFile: '1.3GB',
                numOfFile: 1330,
              ),
              const StorageInfo(
                SvgSrc:'assets/icons/media.svg' ,
                title: 'video',
                amountOfFile: '1.3GB',
                numOfFile: 1330,
              ),
              const StorageInfo(
                SvgSrc:'assets/icons/folder.svg' ,
                title: 'otros archivos',
                amountOfFile: '1.3GB',
                numOfFile: 1330,
              ),
              const StorageInfo(
                SvgSrc:'assets/icons/unknown.svg' ,
                title: 'desconocido',
                amountOfFile: '1.3GB',
                numOfFile: 1330,
              )
            ]
          ),
    );
  }
}

