import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../constant.dart';

class StorageInfo extends StatelessWidget {
  const StorageInfo({
    Key? key, required this.title, required this.SvgSrc, required this.amountOfFile, required this.numOfFile,
  }) : super(key: key);

  final String title, SvgSrc, amountOfFile;
  final int numOfFile;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: defaultPadding),
      padding: const EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: primaryColor.withOpacity(0.15)
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(defaultPadding),
        )
      ),
      child: Row(
        children: [
          SizedBox(
            height: 20,
            width: 20,
            child: SvgPicture.asset(SvgSrc),
          ),
          Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                       Text(
                        title,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text(
                        '$numOfFile archivos',
                        style: Theme.of(context)
                          .textTheme
                          .caption!
                          .copyWith(color: Color(0xFFEEF6F4)),
                      )
                    ],
                  ),
              )
            ),
             Text(amountOfFile)
        ],
      ),
    );
  }
}