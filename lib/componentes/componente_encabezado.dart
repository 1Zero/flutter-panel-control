import 'package:escritorioejemplo/controller/menu_controller.dart';
import 'package:escritorioejemplo/responsive.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import '../constant.dart';

class Encabezado extends StatelessWidget {
  const Encabezado({super.key});

  @override
  Widget build(BuildContext context) {
    return  Row(
              children: [
                if(!Responsive.isDesktop(context)) IconButton(
                    onPressed: context.read<MenuController>().controlMenu, 
                    icon: Icon(Icons.menu)
                  ),
                  if(!Responsive.isMobile(context))
                Text('Tablero',
                  style: Theme.of(context).textTheme.headline6,
                ),
                if(!Responsive.isMobile(context))
                  Spacer(flex:Responsive.isDesktop(context)? 2 : 1,),
                 Expanded(child: searchfield()),
                
                 profilecard()
              ],
            );      
  }
}



Container profilecard() {
    return Container(
                margin:  const EdgeInsets.only(left: defaultPadding),
                padding: const EdgeInsets.symmetric(
                    horizontal:defaultPadding,
                    vertical: defaultPadding / 2, 
                  ),
                decoration:  BoxDecoration(
                  color: secondaryColor,
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  border: Border.all(color: Colors.white10)
                ),
                 child: Row(
                  children: [ 
                    Image.asset(
                        'assets/images/profile_pic.png',
                        height: 38,
                      ),
                        const Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: defaultPadding /2),
                          child: Text('Kallen Kururugi')
                        ),
                     const Icon(Icons.keyboard_arrow_down),   
                  ],
                 ),
               );
  }

  TextField searchfield() {
    return TextField(
                  decoration: InputDecoration(
                    hintText: 'Buscar',
                    fillColor: secondaryColor,
                    filled: true,
                    border: const OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.all(Radius.circular(10))
                    ),
                    suffixIcon: InkWell(
                      onTap: (){},
                      child: Container(padding: const EdgeInsets.all(defaultPadding*0.75),
                        margin: const EdgeInsets.symmetric(
                          horizontal: defaultPadding /2
                        ),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          color:  primaryColor
                        ),
                        child:SvgPicture.asset('assets/icons/Search.svg')  
                      ),
                    )
                  ),
                );
  }