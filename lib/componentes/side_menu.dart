import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SideMenu extends StatelessWidget {
  const SideMenu({super.key});

  @override
  Widget build(BuildContext context) {
    return  Drawer(
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            DrawerHeader(child: Image.asset("assets/images/logo.png")
                            ),
                            DrawerListTile(
                              title: 'Panel Control',
                              SvgSrc: 'assets/icons/menu_dashbord.svg',
                              // ignore: avoid_print
                              press: (){print('panel control');},
                            ),
                            DrawerListTile(
                              title: 'transaccion',
                              SvgSrc: 'assets/icons/menu_tran.svg',
                              press: (){print('transaccion');},
                            ),
                            DrawerListTile(
                              title: 'Tarea',
                              SvgSrc: 'assets/icons/menu_task.svg',
                              press: (){},
                            ),
                            DrawerListTile(
                              title: 'Documentos',
                              SvgSrc: 'assets/icons/menu_doc.svg',
                              press: (){},
                            ),
                            DrawerListTile(
                              title: 'Tienda',
                              SvgSrc: 'assets/icons/menu_store.svg',
                              press: (){},
                            ),
                            DrawerListTile(
                              title: 'Notificasion',
                              SvgSrc: 'assets/icons/menu_notification.svg',
                              press: (){},
                            ),
                            DrawerListTile(
                              title: 'Perfil',
                              SvgSrc: 'assets/icons/menu_profile.svg',
                              press: (){},
                            ),
                            DrawerListTile(
                              title: 'Configuracion',
                              SvgSrc: 'assets/icons/menu_setting.svg',
                              press: (){},
                            ),
                      
                          ],
                        ),
                      ),
                    );
  }
}



class DrawerListTile extends StatelessWidget {
  const DrawerListTile({
     required this.title, required this.SvgSrc, required this.press,
  });

  final String title, SvgSrc;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: press,
      leading: SvgPicture.asset(SvgSrc,
        color: Colors.white,
        height: 16,
      ),
      title: Text(title,
        style: TextStyle(color:Colors.white),
      ),
    );
  }
}