 import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

import '../constant.dart';

SizedBox chart(BuildContext context) {
    return SizedBox(
                              height: 200,
                              child: Stack(
                                children: [
                                  PieChart(
                                    PieChartData(
                                      sectionsSpace: 0,
                                      centerSpaceRadius: 70,
                                      startDegreeOffset: -90,
                                      sections:paichartSelectionData 
                                    )
                                  ),
                                  Positioned.fill(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          const SizedBox(height: defaultPadding,),
                                            Text(
                                              '29.1',
                                              style: Theme.of(context).textTheme
                                              .headline4!.
                                              copyWith(
                                                fontWeight: FontWeight.w600,                                                                                               
                                              ),
                                            ),
                                          Text('de 288')
                                        ]
                                      ),
                                  )
                                ],
                              ),
                            );
  

 
}

 List<PieChartSectionData> paichartSelectionData = [
                                       PieChartSectionData(
                                          color: primaryColor,
                                          value: 25,
                                          showTitle: false,
                                          radius: 25,
                                        ),
                                        PieChartSectionData(
                                          color:Color(0xFF26E5FF),
                                          value: 20,
                                          showTitle: false,
                                          radius: 19,
                                        ),
                                        PieChartSectionData(
                                          color: Color(0xFFFFCF20),
                                          value: 10,
                                          showTitle: false,
                                          radius: 16,
                                        ),
                                        PieChartSectionData(
                                          color: Color(0xFFEDCE9B),
                                          value: 15,
                                          showTitle: false,
                                          radius: 14,
                                        ),
                                        PieChartSectionData(
                                          color: primaryColor.withOpacity(0.1),
                                          value: 15,
                                          showTitle: false,
                                          radius: 10,
                                        ),
                                      ];